part of 'messages_cubit.dart';

@immutable
@JsonSerializable()
class Message {
  @JsonKey(name: "id")
  final String id;

  @JsonKey(name: "dateTime")
  final DateTime dateTime;

  @JsonKey(name: "text")
  final String text;

  const Message({required this.id, required this.dateTime, required this.text});

  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);

  String get represent => '${dateTime.toString()} : $text';

  Map<String, dynamic> toJson() => _$MessageToJson(this);
}

@immutable
@CopyWith()
class MessagesState {
  final Map<String, Message> messagesMap;

  const MessagesState({required this.messagesMap});

  const MessagesState.initial() : messagesMap = const {};
}
