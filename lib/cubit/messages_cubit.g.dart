// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'messages_cubit.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$MessagesStateCWProxy {
  MessagesState messagesMap(Map<String, Message> messagesMap);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `MessagesState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// MessagesState(...).copyWith(id: 12, name: "My name")
  /// ````
  MessagesState call({
    Map<String, Message>? messagesMap,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfMessagesState.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfMessagesState.copyWith.fieldName(...)`
class _$MessagesStateCWProxyImpl implements _$MessagesStateCWProxy {
  const _$MessagesStateCWProxyImpl(this._value);

  final MessagesState _value;

  @override
  MessagesState messagesMap(Map<String, Message> messagesMap) =>
      this(messagesMap: messagesMap);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `MessagesState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// MessagesState(...).copyWith(id: 12, name: "My name")
  /// ````
  MessagesState call({
    Object? messagesMap = const $CopyWithPlaceholder(),
  }) {
    return MessagesState(
      messagesMap:
          messagesMap == const $CopyWithPlaceholder() || messagesMap == null
              ? _value.messagesMap
              // ignore: cast_nullable_to_non_nullable
              : messagesMap as Map<String, Message>,
    );
  }
}

extension $MessagesStateCopyWith on MessagesState {
  /// Returns a callable class that can be used as follows: `instanceOfMessagesState.copyWith(...)` or like so:`instanceOfMessagesState.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$MessagesStateCWProxy get copyWith => _$MessagesStateCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Message _$MessageFromJson(Map<String, dynamic> json) => Message(
      id: json['id'] as String,
      dateTime: DateTime.parse(json['dateTime'] as String),
      text: json['text'] as String,
    );

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'id': instance.id,
      'dateTime': instance.dateTime.toIso8601String(),
      'text': instance.text,
    };
