import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:flutter/widgets.dart';
import 'package:json_annotation/json_annotation.dart';

part 'messages_state.dart';
part 'messages_cubit.g.dart';

class MessagesCubit extends Cubit<MessagesState> {
  MessagesCubit() : super(const MessagesState.initial());

  void receiveRawMessages(
      {required List<QueryDocumentSnapshot<Map<String, dynamic>>> docs}) {
    final newState = {...state.messagesMap}
      ..addAll({for (var doc in docs) doc.id: Message.fromJson(doc.data())});

    emit(state.copyWith(messagesMap: newState));
  }

  void addMessages({required Message message}) {
    final newState = {...state.messagesMap}..addAll({message.id: message});

    emit(state.copyWith(messagesMap: newState));
  }
}
