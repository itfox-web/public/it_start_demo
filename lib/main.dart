import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:it_start/cubit/messages_cubit.dart';

import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MessagesCubit(),
      child: MaterialApp(
        title: 'Flutter Demo 123',
        theme: ThemeData(
          useMaterial3: true,
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(title: 'Flutter Demo Home Page hello!'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final chatCollectionRef = FirebaseFirestore.instance.collection('chat');
  late StreamSubscription<QuerySnapshot<Map<String, dynamic>>>
      snapshotsStreamListener;
  final TextEditingController controller = TextEditingController();

  int counter = 0;

  @override
  void initState() {
    super.initState();

    snapshotsStreamListener = chatCollectionRef.snapshots().listen((event) {
      print(
          'Snapshot collection message: ${event.docs.map((doc) => doc.data())}');

      setState(() {
        counter = event.docs.length;
      });

      context.read<MessagesCubit>().receiveRawMessages(docs: event.docs);
    });
  }

  @override
  void dispose() {
    snapshotsStreamListener.cancel();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: BlocBuilder<MessagesCubit, MessagesState>(
          builder: (context, state) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text(
                  'Messages in chat',
                ),
                Text(
                  'message count: ${state.messagesMap.length}',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                TextFormField(
                  controller: controller,
                ),
                ...state.messagesMap.values.map((message) => Text(
                      message.represent,
                    ))
              ],
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          chatCollectionRef.add(Message(
                  id: 'some_id',
                  dateTime: DateTime.now(),
                  text: controller.text)
              .toJson());
        },
        tooltip: 'Add comment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
