# Что будет в проекте?
* Использование FVM - flutter version manager
* Сервисы Firebase - Firestore
* Подключание к Firestore коллекциям
* Стейт менеджмент через BloC и Cubit
* Работа с Firestore + Сериализация объектов



## Инициализация нового проекта
Используем FVM для того чтобы выбрать окружение и создать новый проект. Подробнее о том как установить FVM - тут: https://fvm.app/docs/getting_started/installation/

```bash
# Инициализируем пустой проект с окружением
fvm use 3.7.0 --force

# Стартуем новый проект 
fvm flutter create it_start
```
Теперь git, обязательно добавляем такую строку в .gitignore этот путь уникальный для каждого рабочего места, не надо его кидать в git `.fvm/flutter_sdk`

``` bash
# Добавляем git
git init
git remote add origin https://gitlab.com/itfox-web/public/it_start_demo.git 
git add .
git push
```

## Сервисы Firebase

Следуем гайду по этой статье и всё будет отлично: https://firebase.google.com/docs/flutter/setup?platform=android
Нам надо добавить в проект поддержку Firestore DB, для этого сначала добавляем зависимость:

```bash
# Добавляем плагины в зависимости
fvm flutter pub add firebase_core cloud_firestore
```

Теперь надо запустить скрипт `flutterfire configure`, выбрать проект firebase и нужные модули. Вообще при каждом изменении которое касается bundle id или состава fireabe плагинов надо вызывать этот скрипт.

```bash
# вызываем скрипт, если надо указываем имеющийся firebase проект или создаем новый
flutterfire configure
```

После того как мы всё скофигурировали надо еще немного доработать `android\app\build.gradle`:
```gradle
...
 defaultConfig {
        applicationId "com.example.app"
       
        // Меняем
        minSdkVersion 19
        multiDexEnabled true
        //

        targetSdkVersion flutter.targetSdkVersion
        versionCode flutterVersionCode.toInteger()
        versionName flutterVersionName
    }
...

dependencies {
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"

    // Добавляем
    implementation 'com.android.support:multidex:1.0.3'
    //
}
...
```

## Подключание к Firestore коллекциям

Чтобы не было проблем с подключением к firestore разрешим всем всё:

0x0:/homepage/developers-wiki/frontend/flutter/new-project-start-retro/.files/image.png

Первым делом инициализируем плагин firebase
```dart
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  runApp(const MyApp());
}
```

Потом определяемся с коллекциями которые мы хотим слушать и подключаемся к стиму с этими коллекциями и обязательно закрываем стрим в `dispose` методе.

```dart
class _MyHomePageState extends State<MyHomePage> {
  final chatCollectionRef = FirebaseFirestore.instance.collection('chat');
  late StreamSubscription<QuerySnapshot<Map<String, dynamic>>>
      snapshotsStreamListener;

  @override
  void initState() {
    super.initState();

    snapshotsStreamListener = chatCollectionRef.snapshots().listen((event) {
      print('Snapshot collection message: ${event.docs.map((doc) => doc.data())}');
    });
  }

  @override
  void dispose() {
    snapshotsStreamListener.cancel();
    super.dispose();
  }
...
```

Теперь при старте приложения или когда в коллекции добаляется или меняется объект мы это сразу увидим.

```
I/flutter (14719): Snapshot collection message: ({datetime: Timestamp(seconds=1682151060, nanoseconds=227000000), text: first_message}, {text: test})
I/flutter (14719): Snapshot collection message: ({datetime: Timestamp(seconds=1682151060, nanoseconds=227000000), text: first_message_changed!}, {text: test})
I/flutter (14719): Snapshot collection message: ({datetime: Timestamp(seconds=1682151060, nanoseconds=227000000), text: first_message_changed!}, {text: test}, {text: new_comment})
```

## Стейт менеджмент через BloC и Cubit

Сначала добавляем зависимости в проект - сам блок, сериализацию/десериализацию в json и расширение copy_with
```bash
# Добавляем зависимости
fvm flutter pub add bloc flutter_bloc json_annotation copy_with_extension dev:build_runner dev:json_serializable dev:copy_with_extension_gen
```

Для хранения состояния сообщений будем использовать Cubit. Это несколько упрощенный паттерн BloC без использования сообщений. Через плагин к VSCode добавляем новый Cubit в проект: `ctrl+shift+p: > Cubit: New cubit`

Сам кубит:
```dart
import 'package:bloc/bloc.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'messages_state.dart';
part 'messages_cubit.g.dart';

class MessagesCubit extends Cubit<MessagesState> {
  MessagesCubit() : super(const MessagesState.initial());
}
```

И State:
```dart
part of 'messages_cubit.dart';

@immutable
@JsonSerializable()
class Message {
  @JsonKey(name: "id")
  final String id;

  @JsonKey(name: "dateTime")
  final DateTime dateTime;

  @JsonKey(name: "text")
  final String text;

  const Message({required this.id, required this.dateTime, required this.text});

  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);

  Map<String, dynamic> toJson() => _$MessageToJson(this);
}

@immutable
@CopyWith()
class MessagesState {
  final Map<String, Message> messagesMap;

  const MessagesState({required this.messagesMap});

  const MessagesState.initial() : messagesMap = const {};
}
```

Добавляем bloc_provider:
```dart
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MessagesCubit(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          useMaterial3: true,
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}
```

Теперь запускаем кодогенерацию:
```bash
fvm flutter pub run build_runner build --delete-conflicting-outputs   
```

Cubit почти готов, осталось только сделать чтобы он умел принимать и хранить сообщения:

```
class MessagesCubit extends Cubit<MessagesState> {
  MessagesCubit() : super(const MessagesState.initial());

  void receiveRawMessages(
      {required List<QueryDocumentSnapshot<Map<String, dynamic>>> docs}) {
    final newState = {...state.messagesMap}
      ..addAll({for (var doc in docs) doc.id: Message.fromJson(doc.data())});

    emit(state.copyWith(messagesMap: newState));
  }

  void addMessages({required Message message}) {
    final newState = {...state.messagesMap}..addAll({message.id: message});

    emit(state.copyWith(messagesMap: newState));
  }
}
```

Ну а вот так мы выводим получившиеся сообщения на форму:
```dart
...
child: BlocBuilder<MessagesCubit, MessagesState>(
          builder: (context, state) {
            return Column(
                ...state.messagesMap.values.map((message) => Text(
                      message.represent,
                    ))
              ],
            );
          },
        ),
...
```
